package com.ingress.jpa.ms7;


import com.ingress.jpa.ms7.domain.*;
import com.ingress.jpa.ms7.respository.*;
import com.ingress.jpa.ms7.services.TableExceptionService;
import com.ingress.jpa.ms7.services.TransferService;
import dto.ResultDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Access;
import javax.persistence.EntityManagerFactory;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@SpringBootApplication
@RequiredArgsConstructor

public class Ms7Application implements CommandLineRunner {


    private final EntityManagerFactory entityManagerFactory;
    private final HGroupsRepository groupsRepository;
    private final HProjectsRepository projectsRepository;
    private final HStudentRepository studentRepository;
    private final TransferService transferService;
    private final AccountRepository accountRepository;
    private final TableExceptionRepository tableExceptionRepository;
    private final TableExceptionService tableExceptionService;

    public static void main(String[] args) {
        SpringApplication.run(Ms7Application.class, args);
    }

    @Override

    public void run(String... args) throws Exception {
        String message = "Test";
        tableExceptionService.customAnnotationWithRollback("with rollback : " + message);
     //   tableExceptionService.customAnnotationNoRollback("no rollback:" + message);



//
//        //Lesson 16
//        Account elshad = new Account();
//        elshad.setName("Elshad");
//        elshad.setBalance(200.0);
//
//        //  accountRepository.save(elshad);
//
//        Account tamerlan = new Account();
//        tamerlan.setName("Tamerlan");
//        tamerlan.setBalance(200.0);
//        //   accountRepository.save(tamerlan);
//
//
//        Account acc1 = accountRepository.findByName(elshad.getName()).get();
//        Account acc2 = accountRepository.findByName(tamerlan.getName()).get();
//
//
//        transferService.transfer(acc1, acc2, 20.0);
//
//
//


//        for (int i = 1; i < 2; i++) {
//
//
//            Student student = new Student();
//            student.setName("Elshad " + i);
//            student.setSurname("Sharbatov " + i);
//
//
//            Projects projects = new Projects();
//            projects.setProjectName("AVIS2 " + String.valueOf(i));
//            projects.setStudent(Set.of(student));
//
//
//            Grope group = new Grope();
//            group.setGroupName("SHV" + i);
//            group.setStudent(Set.of(student));
//
//            student.setGroup(Set.of(group));
//            student.setProjects(Set.of(projects));
//
//
//            projectsRepository.save(projects);
//            groupsRepository.save(group);
//            studentRepository.save(student);
//
//        }

        //working select way1

//        studentRepository.findAllStudentWithConcat()
//                .stream()
//                .map(ResultDto::getResult)
//                .forEach(System.out::println);


        //working select way2 yoxlamali
//        studentRepository.findAllStudentList()
//                .stream().iterator()
//                .forEachRemaining(resultInterface -> {
//                    System.out.println(resultInterface);
//                });

        ;


        //       private void getStudentWithJpaStreamer () {
//        JPAStreamer jpaStreamer = JPAStreamer.of(entityManagerFactory);
//        jpaStreamer
//                .stream(Student.class)
//                .filter(Student$.age.greaterOrEqual(10))
//                .filter(Student$.name.startsWith("Test9"))
//                .filter(Student$.id.isNotNull())
//                .skip(0)
//                .limit(10)
//                .forEach(System.out::println);


    }
}

