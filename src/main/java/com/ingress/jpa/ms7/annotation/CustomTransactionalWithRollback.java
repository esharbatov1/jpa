package com.ingress.jpa.ms7.annotation;


import customsexceptions.CustomExceptionExample;
import org.springframework.transaction.annotation.Transactional;

import java.lang.annotation.*;

@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
@Transactional(rollbackFor = CustomExceptionExample.class)
public @interface CustomTransactionalWithRollback {
}
