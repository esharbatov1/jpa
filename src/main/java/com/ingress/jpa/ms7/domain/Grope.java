package com.ingress.jpa.ms7.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
@Entity
@Table(name = "grope")
@EqualsAndHashCode(exclude = "student")
public class Grope {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String groupName;

    @OneToMany(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "grope_id")
    private Set<Student> student=new HashSet<>();
}
