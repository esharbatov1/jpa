package com.ingress.jpa.ms7.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
@Entity
@EqualsAndHashCode(exclude = "student")
@ToString(exclude = "student")
@Table(name = "projects")
public class Projects {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String projectName;

    @OneToMany(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "projects_id")
    private Set<Student> student=new HashSet<>();


}
