package com.ingress.jpa.ms7.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
@Entity
@Table(name = "student")
@ToString(exclude = "group")


//@EqualsAndHashCode(exclude = "groupe")
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String surname;

    @ManyToMany(cascade = {
            CascadeType.PERSIST
    })
    @JoinColumn(nullable = false)
    private Set<Grope> group;


    @ManyToMany( cascade = {
            CascadeType.PERSIST

    })
    @JoinColumn(nullable = false)
    private Set<Projects> projects=new HashSet<>();

}
