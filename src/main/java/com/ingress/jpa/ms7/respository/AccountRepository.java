package com.ingress.jpa.ms7.respository;

import com.ingress.jpa.ms7.domain.Account;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;


public interface AccountRepository extends JpaRepository<Account, Long> {

    Optional<Account> findByName(String name);
}
