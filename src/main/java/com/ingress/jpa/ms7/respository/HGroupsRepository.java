package com.ingress.jpa.ms7.respository;

import com.ingress.jpa.ms7.domain.Grope;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HGroupsRepository extends JpaRepository<Grope, Long> {
}
