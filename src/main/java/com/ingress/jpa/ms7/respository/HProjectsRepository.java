package com.ingress.jpa.ms7.respository;

import com.ingress.jpa.ms7.domain.Projects;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HProjectsRepository extends JpaRepository<Projects,Long> {
}
