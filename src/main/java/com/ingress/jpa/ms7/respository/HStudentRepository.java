package com.ingress.jpa.ms7.respository;

import com.ingress.jpa.ms7.domain.Student;
import dto.ResultDto;
import dto.ResultInterface;
import dto.ResultsDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;


public interface HStudentRepository extends JpaRepository<Student, Long> {

//    @Query(nativeQuery = true, value = "select a.name as name ,a.surname\n" +
//            "as surname ,group_name as group_name  ,project_name as project_name  from student a\n" +
//            "join projects p on p.id=a.projects_id\n" +
//            "join grope g on g.id=a.grope_id")
//    List<ResultDto> findAllStudentsList();

    @Query(nativeQuery = true,
            value = "select a.name as name ,a.surname\n" +
                    "as surname ,group_name as group_name  ,\n" +
                    "project_name as project_name  from student a\n" +
                    "join projects p on p.id=a.projects_id\n" +
                    "join grope g on g.id=a.grope_id")
    Collection<ResultInterface> findAllStudentList();


    @Query(nativeQuery = true, value = "select concat(a.name ,\" \", a.surname,\" \"\n" +
            ",group_name,\" \",\n" +
            "  project_name )  as result  from student a\n" +
            "join projects p on p.id=a.projects_id\n" +
            "join grope g on g.id=a.grope_id")
    List<ResultDto> findAllStudentWithConcat();

    @Query(nativeQuery = true, value = "select a.name as name ,a.surname\n" +
            "as surname ,group_name as group_name  ,\n" +
            "project_name as project_name  from student a\n" +
            "join projects p on p.id=a.projects_id\n" +
            "join grope g on g.id=a.grope_id")
    List<ResultsDTO> findWithClassQuery();

}
