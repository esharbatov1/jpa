package com.ingress.jpa.ms7.respository;

import com.ingress.jpa.ms7.domain.TableException;
import org.springframework.data.jpa.repository.JpaRepository;


public interface TableExceptionRepository extends JpaRepository<TableException, Long> {


}
