package com.ingress.jpa.ms7.services;


import com.ingress.jpa.ms7.annotation.CustomTransactionalWithNoRollback;
import com.ingress.jpa.ms7.annotation.CustomTransactionalWithRollback;
import com.ingress.jpa.ms7.domain.TableException;
import com.ingress.jpa.ms7.respository.TableExceptionRepository;
import customsexceptions.CustomExceptionExample;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@RequiredArgsConstructor
public class TableExceptionService {
    private final TableExceptionRepository tableExceptionRepository;


    @Transactional
    public void rollbacksOnRuntimeException(String message) {

        TableException tableException = new TableException();
        tableException.setMessage(message);
        tableExceptionRepository.save(tableException);
        throw new RuntimeException("Rollback!");
    }

    @Transactional(rollbackFor = Exception.class)
    public void noRollbackOnCheckedException(String message) throws Exception {
        TableException tableException = new TableException();
        tableException.setMessage(message);
        tableExceptionRepository.save(tableException);
        throw new Exception("Simple exception");
    }


    @CustomTransactionalWithRollback
    public void  customAnnotationWithRollback(String message) throws Exception {
        TableException tableException = new TableException();
        tableException.setMessage(message);
        tableExceptionRepository.save(tableException);
        throw new CustomExceptionExample("Exception:   "+message);
    }
    @CustomTransactionalWithNoRollback
    public void  customAnnotationNoRollback(String message) throws Exception {
        TableException tableException = new TableException();
        tableException.setMessage(message);
        tableExceptionRepository.save(tableException);
        throw new CustomExceptionExample("Exception:   "+message);
    }
}
