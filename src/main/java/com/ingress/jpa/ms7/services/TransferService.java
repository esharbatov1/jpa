package com.ingress.jpa.ms7.services;


import com.ingress.jpa.ms7.domain.Account;
import com.ingress.jpa.ms7.respository.AccountRepository;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Service;

import static java.lang.Thread.sleep;

@Service
@RequiredArgsConstructor
public class TransferService {

    private final AccountRepository accountRepository;

    @SneakyThrows
    public void transfer(Account source, Account target, Double amount) {
        if (source.getBalance() < amount) {
            throw new RuntimeException("Balance not enough");
        }
        source.setBalance(source.getBalance() - amount);
        target.setBalance(target.getBalance() + amount);
        accountRepository.save(source);
        sleep(10000);
        if (true)
            throw new RuntimeException("DB failed");
        accountRepository.save(target);
    }
}
