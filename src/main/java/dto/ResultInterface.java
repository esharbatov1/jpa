package dto;

public interface ResultInterface {
    public String getName();
    public String getSurname();
    public String getGroupName();
    public String getProjectName();

}
