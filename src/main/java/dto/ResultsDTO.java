package dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResultsDTO {
    private String name;
    private String surname;
    private String groupName ;
    private String projectName;
}
